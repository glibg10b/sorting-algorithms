#ifndef H6grkunrL8X3KztFWGTmUAiZ0hFVaVPcWVyUW5Hzk0RwZHTWj8BYAEsN3Ch9b82b7nPPZ
#define H6grkunrL8X3KztFWGTmUAiZ0hFVaVPcWVyUW5Hzk0RwZHTWj8BYAEsN3Ch9b82b7nPPZ

#include "beadsort.h"
#include "bitonicsort.h"
#include "bogosort.h"
#include "countingsort.h"
#include "gnomesort.h"
#include "pancakesort.h"
#include "quicksort.h"
#include "slowsort.h"
#include "spaghettisort.h"
#include "stoogesort.h"

#endif // !H6grkunrL8X3KztFWGTmUAiZ0hFVaVPcWVyUW5Hzk0RwZHTWj8BYAEsN3Ch9b82b7nPPZ
