#ifndef jrWnve3InNTPbcNrupoI60OAJaJPUcg5vv1kiMbwnO4BEbmyoajE68sL6FsXXvO1UDJl4
#define jrWnve3InNTPbcNrupoI60OAJaJPUcg5vv1kiMbwnO4BEbmyoajE68sL6FsXXvO1UDJl4

// Common amongst all sorting functions
#include <functional> // less
#include <random>     // mt19937_64
#include <vector>

#include <cstddef>    // size_t
#include <cmath>      // floor

namespace sort
{
   // Recursively stooge sort array a [l, r]
   template <typename T>
   void stoogeSort(T* a, 
                   long long l, long long r, 
                   std::function<bool(T, T)> cmp = std::less<T>{})
   {
      if (cmp(a[r], a[l])) std::swap(a[r], a[l]);

      if ((r - l) > 1)
      {
         auto third{ std::floor((r - l + 1) / 3) };
         stoogeSort(a, l, r - third, cmp);
         stoogeSort(a, l + third, r, cmp);
         stoogeSort(a, l, r - third, cmp);
      }
   }
   
   // Sort array using Stooge sort
   template <typename T>
   void stooge(std::vector<T>&  input,
      std::mt19937_64&,
      std::function<bool(T, T)> cmp = std::less{}
   )
   {
      stoogeSort(input.data(), 0, static_cast<long long>(input.size() - 1), cmp);
   }
}

#endif // !jrWnve3InNTPbcNrupoI60OAJaJPUcg5vv1kiMbwnO4BEbmyoajE68sL6FsXXvO1UDJl4
