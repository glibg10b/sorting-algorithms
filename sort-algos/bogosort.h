#ifndef dy3lded2CJ3dODbbtDJY9Sy4dnLPxDsHg1hw3MYhxXnlWmBwLLdxSxbh7P3B6tiAgSLkw
#define dy3lded2CJ3dODbbtDJY9Sy4dnLPxDsHg1hw3MYhxXnlWmBwLLdxSxbh7P3B6tiAgSLkw

// Common amongst all sorting functions
#include <functional> // less
#include <random>     // mt19937_64
#include <vector>

#include <algorithm> // is_sorted, shuffle

namespace sort
{
   // Sort input array using Bogo sort
   template <typename T>
   void bogo(std::vector<T>&    input,
      std::mt19937_64&          gen,
      std::function<bool(T, T)> cmp = std::less{}
   )
   {
      while (!std::ranges::is_sorted(input, cmp)) 
         std::ranges::shuffle(input, gen);
   }
}

#endif // !dy3lded2CJ3dODbbtDJY9Sy4dnLPxDsHg1hw3MYhxXnlWmBwLLdxSxbh7P3B6tiAgSLkw
