#ifndef BxbGSDKhS4EpCB3KBVeUhijMcHzqJvGVL2qUKFq5dAi6MCzVyM4LPhFbBrpF7SkSqmzw4
#define BxbGSDKhS4EpCB3KBVeUhijMcHzqJvGVL2qUKFq5dAi6MCzVyM4LPhFbBrpF7SkSqmzw4

// Common amongst all sorting functions
#include <functional> // less
#include <random>     // mt19937_64
#include <vector>

#include <cstddef>    // size_t

namespace sort
{
   // Sort array using Gnome sort
   template <typename T>
   void gnome(std::vector<T>&   input,
      std::mt19937_64&,
      std::function<bool(T, T)> cmp = std::less{}
   )
   {
      std::size_t i{ 0 };
      while (i != input.size())
      {
         if (i == 0.0 || !cmp(input[i], input[i - 1])) ++i;
         else
         {
            std::swap(input[i], input[i - 1]);
            --i;
         }
      }
   }
}

#endif // !BxbGSDKhS4EpCB3KBVeUhijMcHzqJvGVL2qUKFq5dAi6MCzVyM4LPhFbBrpF7SkSqmzw4
