#ifndef ooiN67XbJ6Qu1ADusnDXralGE8MxdxE2kUBEGfSZjvmXS0NFUG6mB9OgDCYQXIg3m5vi9
#define ooiN67XbJ6Qu1ADusnDXralGE8MxdxE2kUBEGfSZjvmXS0NFUG6mB9OgDCYQXIg3m5vi9

// Common amongst all sorting functions
#include <functional> // less
#include <random>     // mt19937_64
#include <vector>

#include <cstddef>    // size_t

namespace sort
{
   // Recursively quick sort (LR pointers, median pivot) array a [lo, hi]
   template <typename T>
   static void lrQuickSort(T* a, 
                           long long lo, long long hi, 
                           std::function<bool(T, T)> cmp = std::less{})
   {
      if (lo >= 0 && hi >= 0 && lo < hi)
      {
         // Getting median this way eliminates overflow
         auto pivot{ a[(hi - lo) / 2 + lo] };
         
         auto l{ lo - 1 };
         auto r{ hi + 1 };

         while (true)
         {
            do { ++l; } while (cmp(a[l], pivot));
            do { --r; } while (cmp(pivot, a[r]));
            if (l >= r) break;
            std::swap(a[l], a[r]);
         }

         lrQuickSort(a, lo, r, cmp);
         lrQuickSort(a, r + 1, hi, cmp);
      }
   }

   // Sort array using Quick sort
   template <typename T>
   void quick(std::vector<T>&   input,
      std::mt19937_64&,
      std::function<bool(T, T)> cmp = std::less{}
   )
   {
      lrQuickSort(input.data(), 0, static_cast<long long>(input.size() - 1), cmp);
   }
}

#endif // !ooiN67XbJ6Qu1ADusnDXralGE8MxdxE2kUBEGfSZjvmXS0NFUG6mB9OgDCYQXIg3m5vi9
