#ifndef aVi0hilkRdrMRd1EYyCXFmuzMIG5tKgkeZ8h9YD890HL5J9sBrzaVDiVk6Q5kBwbJ4Mm4
#define aVi0hilkRdrMRd1EYyCXFmuzMIG5tKgkeZ8h9YD890HL5J9sBrzaVDiVk6Q5kBwbJ4Mm4

// Common amongst all sorting functions
#include <functional> // less
#include <random>     // mt19937_64
#include <vector>

#include <cstddef>    // size_t

namespace sort
{
   // Recursively slow sort array a [l, r]
   template <typename T>
   void slowSort(T* a, 
                   long long l, long long r, 
                   std::function<bool(T, T)> cmp = std::less<T>{})
   {
      if (l >= r) return;

      auto median{ l + (r - l) / 2 };

      slowSort(a, l, median, cmp);
      slowSort(a, median + 1, r, cmp);

      if (cmp(a[r], a[median])) std::swap(a[median], a[r]);

      slowSort(a, l, r - 1, cmp);
   }
   
   // Sort array using Slow sort
   template <typename T>
   void slow(std::vector<T>&    input,
      std::mt19937_64&,
      std::function<bool(T, T)> cmp = std::less{}
   )
   {
      slowSort(input.data(), 0, static_cast<long long>(input.size() - 1), cmp);
   }
}

#endif // !aVi0hilkRdrMRd1EYyCXFmuzMIG5tKgkeZ8h9YD890HL5J9sBrzaVDiVk6Q5kBwbJ4Mm4
