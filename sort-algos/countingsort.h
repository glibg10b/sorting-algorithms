#ifndef ZeZCJQgK6UhQ3iHmUuJZ400NJyBrbDKor1JBwGQUJCJeQKb7Ii7rWGr81jAmV1XRed08i
#define ZeZCJQgK6UhQ3iHmUuJZ400NJyBrbDKor1JBwGQUJCJeQKb7Ii7rWGr81jAmV1XRed08i

// Common amongst all sorting functions
#include <functional> // less
#include <random>     // mt19937_64
#include <vector>

#include <algorithm> // max_element, partial_sum
#include <cstddef>   // size_t

namespace sort
{
   // Counting sort sort array input. Must contain only positive integers.
   template <typename T>
   void counting(std::vector<T>& input,
      std::mt19937_64&,
      std::function<bool(T, T)>  cmp = std::less{} // May not be greater
   )
   {
      std::vector<std::size_t> count{};
      count.resize(static_cast<std::size_t>(*std::ranges::max_element(input, cmp)) + 1);
      std::vector<T>           output{};
      output.resize(input.size());
      for (auto& key: input) ++count[key];

      std::partial_sum(count.begin(), count.end(), count.begin());

      for (auto i{ static_cast<long>(input.size()) - 1 }; i >= 0; --i)
      {
         --count[input[i]];
         output[count[input[i]]] = input[i];
      }

      input = output;
   }
}

#endif // !ZeZCJQgK6UhQ3iHmUuJZ400NJyBrbDKor1JBwGQUJCJeQKb7Ii7rWGr81jAmV1XRed08i
