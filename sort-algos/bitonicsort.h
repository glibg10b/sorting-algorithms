#ifndef gx6EEdUt8BPbK9AfhCRFzMcH9ndDtWGiUlhVCy5yEbehsktUDLrLYvHa21DRZRsxIvlVB
#define gx6EEdUt8BPbK9AfhCRFzMcH9ndDtWGiUlhVCy5yEbehsktUDLrLYvHa21DRZRsxIvlVB

// Common amongst all sorting functions
#include <functional> // less
#include <random>     // mt19937_64
#include <vector>

#include <algorithm>  // swap
#include <cstddef>    // size_t

namespace sort
{
   // Sort array input using the bitonic sort algorithm. input.size() must be a power of 2.
   template <typename T>
   void bitonic(std::vector<T>& input,
      std::mt19937_64&,
      std::function<bool(T, T)> cmp = std::less{} // Ignored
   )
   {
      for (std::size_t k{ 2 }; k <= input.size(); k *= 2)
         for (auto j{ k / 2 }; j > 0; j /= 2)
            for (std::size_t i{ 0 }; i < input.size(); ++i)
            {
               auto l{ i ^ j };
               if (l > i)
                  if ((i & k) == 0 && less(input[l], input[i])
                   || (i & k) != 0 && less(input[i], input[l]))
                     std::swap(input[i], input[l]);
            }
   }
}

#endif // !gx6EEdUt8BPbK9AfhCRFzMcH9ndDtWGiUlhVCy5yEbehsktUDLrLYvHa21DRZRsxIvlVB
