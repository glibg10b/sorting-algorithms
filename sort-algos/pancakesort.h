#ifndef KCc1cWDnT6VvAcEbHsrZklT2rMAl0v1MkopZUJFGaV86VzC2sqgSOUxqmNJH2BRCeNU9q
#define KCc1cWDnT6VvAcEbHsrZklT2rMAl0v1MkopZUJFGaV86VzC2sqgSOUxqmNJH2BRCeNU9q

// Common amongst all sorting functions
#include <functional> // less
#include <random>     // mt19937_64
#include <vector>

#include <algorithm>  // max_element, reverse

namespace sort
{
   // Pancake sort array
   template <typename T>
   void pancake(std::vector<T>& input,
      std::mt19937_64&,
      std::function<bool(T, T)> cmp = std::less{}
   )
   {
      for (auto i{ input.size() }; i > 1; --i)
      {
         // Iterator to first largest element
         auto max{ std::max_element(input.begin(), input.begin() + static_cast<long>(i), cmp) };

         std::reverse(input.begin(), max + 1);
         std::reverse(input.begin(), input.begin() + static_cast<long>(i));
      }
   }
}

#endif // !KCc1cWDnT6VvAcEbHsrZklT2rMAl0v1MkopZUJFGaV86VzC2sqgSOUxqmNJH2BRCeNU9q
