#ifndef R1xW2pEivpkaquNWU4ho3ZIfYFsO0vHBATfE3JPJMmBR8wNja17OCYfhG3a2t9ps50NuW
#define R1xW2pEivpkaquNWU4ho3ZIfYFsO0vHBATfE3JPJMmBR8wNja17OCYfhG3a2t9ps50NuW

// Common amongst all sorting functions
#include <functional> // less
#include <random>     // mt19937_64
#include <vector>

#include <algorithm>  // max_element
#include <cstddef>    // size_t
#include <numeric>    // accumulate

namespace sort
{
   // Bead sort array input (descending). Must contain only positive integers.
   template <typename T>
   void bead(std::vector<T>&    input,
      std::mt19937_64&,
      std::function<bool(T, T)> cmp = std::less{} // May not be greater
   )
   {
      // The vertical poles
      std::vector<T> poles;
      poles.resize(static_cast<std::size_t>(*std::ranges::max_element(input, cmp)));

      for (auto row: input) 
         for (std::size_t i{ 0 }; i < static_cast<std::size_t>(row); ++i) ++(poles[i]);

      for (std::size_t row{ 0 }; row < input.size(); ++row)
         // Reduce doesn't work and is about the same speed for some reason
         input[row] = std::accumulate(poles.begin(), poles.end(), static_cast<T>(0),
            [row](T init, T last)
            {
               return init + (static_cast<std::size_t>(last) > row);
            }
         );
   }
}

#endif // !R1xW2pEivpkaquNWU4ho3ZIfYFsO0vHBATfE3JPJMmBR8wNja17OCYfhG3a2t9ps50NuW
