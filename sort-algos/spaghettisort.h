#ifndef Zdlarxjt1WUgWDGWPYD8SfcYXXIX0RLxtdIUmp4SbBGPRJNw9CEOevjRajNw1FI9nVKtc
#define Zdlarxjt1WUgWDGWPYD8SfcYXXIX0RLxtdIUmp4SbBGPRJNw9CEOevjRajNw1FI9nVKtc

// Common amongst all sorting functions
#include <functional> // less
#include <random>     // mt19937_64
#include <vector>

#include <cstddef> // size_t
#include <limits> // numeric_limits::[min, max, epsilon]

namespace sort
{
   // Simulate Spaghetti sort on array
   template <typename T>
   void spaghetti(std::vector<T>& input,
      std::mt19937_64&,
      std::function<bool(T, T)>   cmp = std::less{} // May not be greater
   )
   {
      std::vector<T> output;
      output.resize(input.size());

      // Hand starting height
      auto start{ std::numeric_limits<T>::max() };

      // Hand final height
      auto end{ std::numeric_limits<T>::min() };

      // Hand height step size
      // epsilon() returns 0 for integers, so we use 1 for them.
      auto step{ std::numeric_limits<T>::is_integer? 1 : std::numeric_limits<T>::epsilon() };

      // Output array index
      auto o{ output.size() - 1 };
      for (auto height{ start }; height > end && !input.empty(); height -= step)
         for (std::size_t i{ 0 }; i < input.size(); ++i)
            if (!cmp(input[i], height))
            {
               output[o] = input[i];
               input.erase(input.begin() + static_cast<long int>(i));
               --i; // So we don't skip the next element
               --o;
            }

      input = output;
   }
}

#endif // !Zdlarxjt1WUgWDGWPYD8SfcYXXIX0RLxtdIUmp4SbBGPRJNw9CEOevjRajNw1FI9nVKtc
