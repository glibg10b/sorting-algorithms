# Sorting algorithms

## Algorithms

All algorithms are in [sort-algos/](https://gitlab.com/glibg10b/sorting-algorithms/-/tree/main/sort-algos) in namespace `sort`. [sort.h](https://gitlab.com/glibg10b/sorting-algorithms/-/blob/main/sort.h) includes all of them.

- [Quicksort](https://en.wikipedia.org/wiki/Quicksort) (quicksort.h)
- [Gnome sort](https://en.wikipedia.org/wiki/Gnome_sort) (gnomesort.h)
- [Counting sort](https://en.wikipedia.org/wiki/Counting_sort) (countingsort.h)
- [Spaghetti (Poll) sort](https://en.wikipedia.org/wiki/Spaghetti_sort) (spaghettisort.h)
- [Bitonic sorter](https://en.wikipedia.org/wiki/Bitonic_sorter) (bitonicsort.h)
- [Bogosort](https://en.wikipedia.org/wiki/Bogosort) (bogosort.h)
- [Bead sort](https://en.wikipedia.org/wiki/Bead_sort) (beadsort.h)
- [Simple pancake sort](https://en.wikipedia.org/wiki/Pancake_sorting) (pancakesort.h)
- [Stooge sort](https://en.wikipedia.org/wiki/Stooge_sort) (stoogesort.h)
- [Slow sort](https://en.wikipedia.org/wiki/Slowsort) (slowsort.h)
